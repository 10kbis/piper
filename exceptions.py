class PiperError(Exception):
    def __init__(self, msg, sourceposes=None):
        self.msg = msg
        self.sourceposes = sourceposes or []

    def get_error(self, source):
        errs = []
        err_line_prefix = ' {} | '
        for sp in self.sourceposes:
            start_line = source.rfind('\n', 0, sp.start) + 1
            end_line = source.find('\n', sp.start)
            if end_line == -1:
                end_line = len(source)
            location = source[start_line:end_line]
            end_location = end_line if sp.end > end_line else sp.end

            prefix = err_line_prefix.format(sp.line)

            offset = sp.start - start_line
            underline = ' ' * (offset + len(prefix)) + '^' * (end_location - sp.start)

            err = prefix + location + '\n' + underline
            errs.append(err)
        
        return '\n'.join(errs) + '\n' + self.msg


class MismatchParameter(Exception):
    pass


class ErroneousActorDefinition(Exception):
    def __init__(self, actor_name, msg):
        self.actor_name = actor_name
        self.msg = msg

    def get_error(self):
        return f"Errorneous definition for actor {self.actor_name}:\n\t{self.msg}"


class PiperRuntimeError(Exception):
    pass


class PiperUnexpectedType(PiperRuntimeError):
    def __init__(self, expected, got):
        self.expected = expected
        self.got = got


class BinaryOpWrongType(PiperRuntimeError):
    def __init__(self, left, right):
        self.left = left
        self.right = right

class WrongIndex(PiperRuntimeError):
    def __init__(self, list_, at):
        self.list_ = list_
        self.at = at


class WrongIndexType(WrongIndex):
    pass

class WrongIndexValue(WrongIndex):
    pass
