import parser

from context import get_builtin_context
from exceptions import ErroneousActorDefinition, PiperError

try:
    context = get_builtin_context()
except ErroneousActorDefinition as e:
    print(e.get_error())
    exit(1)

f = open('test.pp')
content = f.read()
try:
    program = parser.parse(content)
    program.eval(context)
except PiperError as e:
    print(e.get_error(content))
    exit(1)


"""
- Everything breaks when a # is inserted inside a string, the parser thinks it is the start of a comment
- When too many arguments are given, print signature of the actor in the error
- Print values inside objects as literals
- ls follow link could result in infinite loop ?
"""


"""
ls -f |
    groupby ($f: $f.ext) --key "ext" --values "files" |
    { $x.ext, count = $x.files.length(), size = $($x.files >> $x.size | sum) } |
    sort ($grp: $grp.size) |
    "ext={$x.ext}, {$($x.size >> unit "storage")} ({$x.count} files)"
"""
