import itertools
import os
from collections import defaultdict
from decimal import Decimal
from pathlib import Path

from actor import actor, arg, producer, trans_producer, transformer
from exceptions import PiperUnexpectedType, PiperRuntimeError
from piper_types import *


@arg("only_files", bool, "Lists only files", short="f", default=False)
@arg("only_dirs", bool, "Lists only directories", short="d", default=False)
@arg("recursive", bool, "Recursively iterate", short="r", default=False)
@arg("path", Path, "Path to list", default=Path("."), positional=True)
@producer("List files and directories")
def ls(path, recursive, only_dirs, only_files):
    show_all = (not only_files) and (not only_dirs)
    dirs = [path]
    while dirs:
        d = dirs.pop()
        # os.scandir caches the node type (file, directory, etc).
        # This means it is ok to call is_file/is_dir multiple times
        # when it comes to performance
        with os.scandir(d) as it:
            for entry in sorted(it, key=lambda x: x.path):
                if show_all:
                    yield Path(entry.path)
                elif only_dirs and entry.is_dir():
                    yield Path(entry.path)
                elif only_files and entry.is_file():
                    yield Path(entry.path)
                
                if recursive and entry.is_dir():
                    dirs.append(Path(entry.path))


@arg("times", Decimal, "Number of time to repeat", default=Decimal(2), positional=True)
@transformer("Repeats every item a given number of times (2 by default)")
def repeat(times, stdin):
    for item in stdin:
        for _ in range(int(times)):
            yield item


@arg("start", int, "Start of the range (inclusive)", default=0)
@arg("list_", PiperList, "List to enumerate", default=PiperEmptyList, positional=True)
@trans_producer("Enumerate input elements")
def enum(list_, start, stdin):
    if list_ and stdin:
        raise PiperRuntimeError("enum is used as a transformer and a producer at the same time")
    
    if stdin:
        list_ = stdin

    for i, value in enumerate(list_):
        yield {'i': PiperNumber(i), 'value': value}


@arg("reverse", bool, "Sorts in descending (largest to smallest) order", default=False)
@arg("key", PiperLambda, "", positional=True, default=PiperLambdaId)
@transformer("Sorts items in ascending (smallest to largest) order")
def sort(key, reverse, stdin):
    if key is PiperLambdaId:
        yield from sorted(stdin, reverse=reverse)
    else:
        yield from sorted(stdin, key=key, reverse=reverse)

@arg("adjacent", bool, "Only remove adjacent duplicate items", default=False)
@arg("key", PiperLambda, "", positional=True, default=PiperLambdaId)
@transformer("Removes duplicate items")
def unique(key, adjacent, stdin):
    if adjacent:
        prev = None
        prev_key = None
        for item in stdin:
            item_key = key(item)
            if prev is None or item_key != prev_key:
                yield item
                prev = item
                prev_key = item_key
    else:
        seen = set()
        for item in stdin:
            item_key = key(item)
            if item_key not in seen:
                seen.add(item_key)
                yield item


@arg("step", Decimal, "Increment step", default=Decimal(1))
@arg("start", Decimal, "Start of the range (inclusive)", default=Decimal(0), positional=True)
@arg("stop", Decimal, "Stop of the range (exclusive)", positional=True)
@producer("Generates a range from 'start' to 'stop' with an increment of 'step'")
def count(start, stop, step):
    n = start
    if step > 0:
        while n < stop:
            yield n
            n += step
    else:
        while n > stop:
            yield n
            n += step



@arg("step", Decimal, "Increment step", default=Decimal(1))
@arg("stop", Decimal, "Stop of the range (exclusive)", positional=True)
@arg("start", Decimal, "Start of the range (inclusive)", positional=True)
@producer("Generates a range from 'start' to 'stop' with an increment of 'step'", name="range")
def range_(start, stop, step):
    yield from count(start, stop, step)


@transformer("Prints the values")
def echo(stdin):
    for value in stdin:
        print(value)
        yield value


@arg("predicate", PiperLambda, "", positional=True)
@transformer("Keeps items for which the predicate is true")
def select(predicate, stdin):
    for item in stdin:
        if predicate(item):
            yield item


@arg("predicate", PiperLambda, "", positional=True)
@transformer("Keeps items as long as the predicate is true")
def takewhile(predicate, stdin):
    for item in stdin:
        if not predicate(item):
            return
        yield item


@arg("predicate", PiperLambda, "", positional=True)
@transformer("Drops items as long as the predicate is true and returns every items afterwards")
def dropwhile(predicate, stdin):
    stop_dropping = False
    for item in stdin:
        if stop_dropping:
            yield item
        elif not predicate(item):
            stop_dropping = True
            yield item


@arg("func", PiperLambda, "", positional=True)
@transformer("")
def apply(func, stdin):
    for item in stdin:
        yield func(item)


@arg("separator", str, "Line separator", default='\n', short='s')
@arg("path", Path, "File to read", positional=True)
@producer("")
def readlines(path, separator):
    buffsize = 1024
    prev = ''
    f = open(path)

    while True:
        s = f.read(buffsize)
        if not s:
            if prev:
                yield prev
            return
        split = s.split(separator)
        if len(split) > 1:
            yield prev + split[0]
            yield from split[1:-1]
            prev = split[-1]
        else:
            prev += s


@arg("append", bool, "Append to file", default=False, short='a')
@arg("separator", str, "Line separator", default='\n', short='s')
@arg("path", Path, "File to read", positional=True)
@transformer("")
def writelines(path, separator, append, stdin):
    f = open(path, 'at' if append else 'wt')
    for item in stdin:
        f.write(item._piper_format())
        f.write(separator)
        yield item


@arg("parent", bool, "Create parent directories if needed", default=False, short="p")
@arg("path", Path, "Directory to create", positional=True)
@producer("")
def mkdir(path, parent):
    path.mkdir(parents=parent)


@arg("values", str, "", default="values", short="v")
@arg("key", str, "", default="key", short="k")
@arg("func", PiperLambda, "", positional=True)
@transformer("")
def groupby(func, key, values, stdin):
    key_name = key
    values_name = values
    values = defaultdict(list)
    for key, group in itertools.groupby(stdin, key=func):
        values[key].extend(group)
    
    for key, group in values.items():
        yield {key_name: key, values_name: PiperList(group)}


def sum_count(stdin):
    total = PiperNumber(0)
    n = 0
    for item in stdin:
        if not isinstance(item, PiperNumber):
            raise PiperUnexpectedType(PiperNumber, type(item))
        total += item
        n += 1

    return total, PiperNumber(n)


@transformer("Sums the items", name="sum")
def sum_(stdin):
    yield sum_count(stdin)[0]


@transformer("Averages the items")
def average(stdin):
    total, count = sum_count(stdin)
    yield total / count


@arg("no_sort", bool, "Do not sort the items", default=False)
@arg("key", PiperLambda, "key lambda to use when sorting items", positional=True, default=PiperLambdaId)
@transformer("Returns the median")
def median(key, no_sort, stdin):
    items = list(stdin if no_sort else sorted(stdin, key=key))
    n = len(items)
    if n != 0:
        yield items[n // 2]


@arg("long", bool, "Format with long names (eg. 'megabyte' instead of 'MB')", default=False)
@arg("unit_type", str, "Possible values 'storage', 'storage_binary', 'bitrate'", positional=True)
@transformer("")
def unit(unit_type, long, stdin):
    units = {
        # From https://en.wikipedia.org/wiki/Binary_prefix
        'storage': (
            1000,
            ('B', ('', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')),
            ('byte', ('', 'kilo', 'mega', 'giga', 'tera', 'peta', 'exa', 'zetta', 'yotta'))
        ),
        'storage_binary': (
            1024,
            ('B', ('', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi')),
            ('byte', ('', 'kibi', 'mebi', 'gibi', 'tebi', 'pebi', 'exbi', 'zebi', 'yobi'))
        ),
        'bitrate': (
            1000,
            ('bps', ('', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')),
            ('bit/s', ('', 'kilo', 'mega', 'giga', 'tera', 'peta', 'exa', 'zetta', 'yotta'))
        )
    }
    base, shorts, longs = units[unit_type]
    units = longs if long else shorts
    suffix, prefixes = units
    units = [prefix + suffix for prefix in prefixes]

    def convert(num):
        if not isinstance(num, PiperNumber):
            raise PiperUnexpectedType(PiperNumber, type(num))

        num = num._piper_unwrap()
        for unit in units:
            if abs(num) < base:
                return f"{num:.3g} {unit}"
            num /= base
        return f"{num:.3g} {units[-1]}"

    yield from map(convert, stdin)


@transformer("Convert integer to char", name="chr")
def chr_(stdin):
    for value in stdin:
        yield chr(int(value._piper_as_number()))

@arg("sep", str, "Separator", positional=True)
@transformer("Convert items to a string")
def join(sep, stdin):
    yield sep.join(map(lambda x: x._piper_as_string(), stdin))