from abc import ABC, abstractmethod
from decimal import Decimal
from pathlib import Path
from types import FunctionType, MethodType

from exceptions import BinaryOpWrongType, WrongIndexType, WrongIndexValue


class ImpossibleImplicitConversion(Exception):
    pass


class PiperAny(ABC):
    _piper_type_name = "Any"

    @abstractmethod
    def _piper_unwrap(self):
        ...

    @abstractmethod
    def _piper_format(self) -> str:
        ...

    @abstractmethod
    def __hash__(self):
        ...

    @abstractmethod
    def __eq__(self, other):
        ...

    def __bool__(self):
        return True

    def _piper_as_bool(self):
        raise ImpossibleImplicitConversion()

    def _piper_as_number(self):
        raise ImpossibleImplicitConversion()

    def _piper_as_string(self):
        raise ImpossibleImplicitConversion()

    # If __lt__ and __eq__ are implemented then __ne__, __le__, __ge__, __gt__
    # can be derived
    def __ne__(self, other):
        return not self == other

    def __le__(self, other):
        return self == other or self < other

    def __gt__(self, other):
        return not (self <= other)

    def __ge__(self, other):
        return not (self < other)

    def __add__(self, other):
        raise BinaryOpWrongType(type(self), type(other))
    
    __sub__ = __mul__ = __floordiv__ = __truediv__ = __mod__ = __pow__ = __contains__ = __getitem__ = __add__


class PiperObject(PiperAny):
    __slots__ = ('__fields',)
    _piper_type_name = "Object"

    def __init__(self, fields: dict):
        self.__fields = fields

    def _piper_unwrap(self):
        return self.__fields
    
    def __repr__(self):
        return "Object(" + self._piper_format() + ")"

    def _piper_format(self) -> str:
        return "{" + ", ".join(f"{name} = {value._piper_format()}" for name, value in self.__fields.items()) + "}"

    def __hash__(self):
        return hash(tuple(self.__fields.items()))

    def __eq__(self, other):
        if other.__class__ != PiperObject:
            return False
        if len(self.__fields) != len(other.fields):
            return False
        for name, value in self.__fields.items():
            o_value = other.fields.get(name)
            if o_value != value:
                return False
        return True
    
    def __getitem__(self, name):
        if not isinstance(name, PiperString):
            raise WrongIndexType(self, name)
        try:
            return self.__fields[name._piper_unwrap()]
        except KeyError:
            raise WrongIndexValue(self, name)
        
    def __getattr__(self, attr):
        try:
            return self.__fields[attr]
        except KeyError:
            raise AttributeError()

    def __bool__(self):
        return bool(self.__fields)


class PiperList(PiperAny):
    __slots__ = ('__values',)
    _piper_type_name = "List"

    def __init__(self, values: list):
        self.__values = values

    def _piper_unwrap(self):
        return self.__values
    
    def __repr__(self):
        return "List(" + self._piper_format() + ")"

    def _piper_format(self) -> str:
        return "[" + ", ".join(f"{value._piper_format()}" for value in self.__values) + "]"
    
    def _piper_iter(self):
        yield from self.__values

    def __hash__(self):
        return hash(tuple(self.__values))

    def __eq__(self, other):
        if other.__class__ != PiperList:
            raise BinaryOpWrongType(type(self), type(other))
        return self.__values == other.__values
    
    def __lt__(self, other):
        if other.__class__ != PiperList:
            raise BinaryOpWrongType(type(self), type(other))
        return self.__values < other.__values
    
    def __len__(self):
        return len(self.__values)
    
    def __getitem__(self, index):
        if not isinstance(index, PiperNumber):
            raise WrongIndexType(self, index)
        elif not index._piper_is_int():
            raise WrongIndexValue(self, index)
        try:
            return self.__values[index._piper_to_int()]
        except IndexError:
            raise WrongIndexValue(self, index)
    
    def __iter__(self):
        yield from self.__values

    def __bool__(self):
        return bool(self.__values)
    
    def length(self):
        return PiperNumber(len(self.__values))


class PiperBool(PiperAny):
    __slots__ = ('__value',)
    _piper_type_name = "Bool"

    def __init__(self, value):
        self.__value = value

    def __repr__(self):
        return f"PiperBool({self.__value})"

    def __hash__(self):
        return hash(self.__value)

    def __eq__(self, other):
        try:
            return PiperBool(self.__value == other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __lt__(self, other):
        try:
            return PiperBool(self.__value < other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __add__(self, other):
        try:
            return PiperNumber(self.__value + other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __sub__(self, other):
        try:
            return PiperNumber(self.__value - other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __mul__(self, other):
        try:
            return PiperNumber(self.__value * other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __floordiv__(self, other):
        try:
            return PiperNumber(self.__value // other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __truediv__(self, other):
        try:
            return PiperNumber(self.__value / other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __mod__(self, other):
        try:
            return PiperNumber(self.__value % other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))

    def __pow__(self, other):
        try:
            return PiperNumber(self.__value ** other._piper_as_bool())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperBool, type(other))
    
    def __not__(self):
        return PiperBool(not self.__value)
    
    def __bool__(self):
        return self.__value

    def _piper_as_bool(self):
        return self.__value

    def _piper_as_number(self):
        return 1 if self.__value else 0

    def _piper_format(self) -> str:
        return "true" if self.__value else "false"

    def _piper_unwrap(self):
        return self.__value


class PiperString(PiperAny):
    __slots__ = ('__value',)
    _piper_type_name = "String"

    def __init__(self, value: str):
        self.__value = value

    def __repr__(self):
        return f"PiperString({self.__value})"

    def __hash__(self):
        return hash(self.__value)

    def __eq__(self, other):
        try:
            return PiperBool(self.__value == other._piper_as_string())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperString, type(other))

    def __lt__(self, other):
        try:
            return PiperBool(self.__value < other._piper_as_string())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperString, type(other))

    def __add__(self, other):
        try:
            return PiperString(self.__value + other._piper_as_string())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperString, type(other))

    def __mul__(self, other):
        try:
            n = other._piper_as_number()
            if type(n) != int:
               raise ImpossibleImplicitConversion()
            return PiperString(self.__value * n)
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperString, type(other))
    
    def __getitem__(self, index):
        if not isinstance(index, PiperNumber):
            raise WrongIndexType(self, index)
        elif not index._piper_is_int():
            raise WrongIndexValue(self, index)
        try:
            return self.__value[index._piper_to_int()]
        except IndexError:
            raise WrongIndexValue(self, index)
    
    def __iter__(self):
        yield from self.__value
    
    def __bool__(self):
        return self.__value != ""

    def _piper_as_bool(self):
        return self.__value != ""

    def _piper_as_string(self):
        return self.__value

    def _piper_format(self) -> str:
        return self.__value

    def _piper_unwrap(self):
        return self.__value
    
    def length(self):
        return PiperNumber(len(self.__value))


class PiperNumber(PiperAny):
    __slots__ = ('__value',)
    _piper_type_name = "Number"

    def __init__(self, value):
        try:
            self.__value = int(value)
        except ValueError:
            self.__value = Decimal(value)

    def __repr__(self):
        return f"PiperNumber({self.__value})"

    def __hash__(self):
        return hash(self.__value)

    def __eq__(self, other):
        try:
            return PiperBool(self.__value == other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __lt__(self, other):
        try:
            return PiperBool(self.__value < other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __add__(self, other):
        try:
            return PiperNumber(self.__value + other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __sub__(self, other):
        try:
            return PiperNumber(self.__value - other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __mul__(self, other):
        try:
            return PiperNumber(self.__value * other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __floordiv__(self, other):
        try:
            return PiperNumber(self.__value // other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __truediv__(self, other):
        try:
            result = self.__value / other._piper_as_number()
            if type(result) == float:
                # The result is not an integer so cast one of the operand to
                # ensure we have a Decimal result
                return PiperNumber(Decimal(self.__value) / other._piper_as_number())
            return PiperNumber(result)
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __mod__(self, other):
        try:
            return PiperNumber(self.__value % other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))

    def __pow__(self, other):
        try:
            return PiperNumber(self.__value ** other._piper_as_number())
        except ImpossibleImplicitConversion:
            raise BinaryOpWrongType(PiperNumber, type(other))
    
    def __neg__(self):
        return PiperNumber(-self.__value)
    
    def __bool__(self):
        return self.__value != 0

    def _piper_as_bool(self):
        return bool(self.__value)

    def _piper_as_number(self):
        return self.__value

    def _piper_format(self) -> str:
        return str(self.__value)

    def _piper_unwrap(self):
        return self.__value
    
    def _piper_to_int(self):
        return int(self.__value)
    
    def _piper_is_int(self):
        return self.__value == int(self.__value)
    
    def is_int(self):
        return PiperBool(self.__value == int(self.__value))
    
    def to_int(self):
        return PiperNumber(int(self.__value))
    
    def is_even(self):
        return self.is_int() and PiperBool(self.__value % 2 == 0)
    
    def is_odd(self):
        return PiperBool(not self.is_even())


class PiperPath(PiperAny):
    __slots__ = ('__value',)
    _piper_type_name = "Path"

    def __init__(self, value: str):
        self.__value = Path(value)

    def __repr__(self):
        return f"PiperPath({self.__value})"

    def __eq__(self, other):
        if not isinstance(other, PiperPath):
            raise BinaryOpWrongType(PiperPath, type(other))
        return PiperBool(self.__value == other.__value)

    def __lt__(self, other):
        if not isinstance(other, PiperPath):
            raise BinaryOpWrongType(PiperPath, type(other))
        return PiperBool(self.__value < other.__value)

    def __truediv__(self, other):
        if not isinstance(other, PiperPath):
            raise BinaryOpWrongType(PiperPath, type(other))
        return PiperPath(self.__value / other.__value)

    def _piper_as_string(self):
        return str(self.__value)

    def _piper_format(self) -> str:
        return str(self.__value)

    def _piper_unwrap(self):
        return self.__value

    @property
    def ext(self) -> PiperString:
        return PiperString("".join(self.__value.suffixes))

    @property
    def suffix(self) -> PiperString:
        return PiperString(self.__value.suffix)

    @property
    def suffixes(self) -> PiperList:
        return PiperList(list(PiperString(sfx for sfx in self.__value.suffixes)))
    
    @property
    def size(self) -> PiperNumber:
        return PiperNumber(self.__value.stat().st_size)

    def is_file(self) -> PiperBool:
        return PiperBool(self.__value.is_file())


class PiperLambda(PiperAny):
    __slots__ = ('__value',)
    _piper_type_name = "Lambda"

    def __init__(self, value):
        self.__value = value
    
    def __call__(self, *args):
        return self.__value(*args)
    
    def __eq__(self, other):
        return self.__value == other.__value
    
    def __hash__(self):
        return hash(self.__value)

    def _piper_format(self) -> str:
        return '<Lambda>'

    def _piper_unwrap(self):
        return self.__value


PiperLambdaId = PiperLambda(lambda x: x)
PiperEmptyList = PiperList([])

python_piper_type_mapping = {
    int: PiperNumber,
    Decimal: PiperNumber,
    bool: PiperBool,
    str: PiperString,
    Path: PiperPath,
    FunctionType: PiperLambda,
    MethodType: PiperLambda,
    dict: PiperObject,
    list: PiperList,
}


def is_python_value(value):
    return is_python_type(type(value))


def is_python_type(type_):
    return not issubclass(type_, PiperAny)


def wrap_python_value(value):
    type_ = type(value)
    for py_type, piper_type in python_piper_type_mapping.items():
        if issubclass(type_, py_type):
            return piper_type(value)

    raise ValueError(
        f"Impossible to convert this Python value '{value}' to a Piper value"
    )

def wrap_python_list(list_):
    return PiperList([wrap_python_value(value) for value in list_])
