$array = [1, 3, 3, 6, 7, 8, 9]
# # $array >> | {n = $x} | writelines ./test.txt --append --separator "lol"
# # readlines ./test.txt -s "="
# # $array >> 2 ^ $x
# $n = 95
# $m = $n - 2
# count $n --start $m | 2 ^ $x

# readlines ./test.txt | select ($l: $l.length() > 18)
# enum $args | "argv[{$x.i}] = {$x.value}"
# $args >> "argv[{$i}] = $x"
# $objs = {x = {a = 1}, y = {a = 42, b = 69}}
# $objs >> {$x.x, $x.y.a, $x.y.a, a = 69}
# enum $args | {    $x.i,    count = $x.value.length()}

# $m=$args[0]
# $l=$m.length()
# $h="@"*($l+4)
# $j="@{" "*($l+2)}@"
# [$h,$j,"@ {$m} @",$j,$h]>>$x


# $a="ZYXWVUTSRQPONMLKJIHGFEDCBA"
# $a=$(range 64 90|ord|join "")

$a = 1
range 26 0 --step -$a