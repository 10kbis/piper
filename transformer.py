import operator
import traceback

from lark import Token, Transformer

from exceptions import (BinaryOpWrongType, PiperError, PiperRuntimeError,
                        PiperUnexpectedType)
from piper_types import *


def deescape_string(s):
    patterns = [
        ('\\\\', '\\'),
        ('\\n', '\n'),
        ('\\t', '\t'),
        ('\\{', '{'),
        ('\\}', '}'),
        ('\\"', '"'),
    ]
    for pattern, replace in patterns:
        s = s.replace(pattern, replace)
    return s


class SourcePos:
    def __init__(self, token=None, start=None, end=None, column=None, line=None):
        if token is not None:
            self.start = token.pos_in_stream
            self.end = token.end_pos
            self.column = token.column
            self.line = token.line
        else:
            self.start = start
            self.end = end
            self.column = column
            self.line = line
        
    def __repr__(self):
        return f"SourcePos(start={self.start}, end={self.end}, column={self.column}, line={self.line})"

    def extend_to(self, srcpos):
        return SourcePos(
            start=self.start, end=srcpos.end, column=self.column, line=self.line
        )


class Program:
    def __init__(self, statements):
        self.statements = statements

    def __repr__(self):
        return "\n".join(map(repr, self.statements))

    def eval(self, context):
        for stmt in self.statements:
            if isinstance(stmt, Stream):
                for item in stmt.eval(context):
                    print(item._piper_format())
            elif isinstance(stmt, Assign):
                stmt.eval(context)


class Stream:
    def __init__(self, cmds):
        self.cmds = cmds

    def __repr__(self):
        return " | ".join(map(repr, self.cmds))

    def eval(self, context):
        prev_generator = None
        for cmd in self.cmds:
            generator = cmd.eval(context, prev_generator)
            prev_generator = generator
        
        # Must always have a command in a stream
        assert prev_generator is not None

        yield from generator


class SubStream(Stream):
    def __init__(self, stream, srcpos):
        self.stream = stream
        self.srcpos = srcpos
    
    def __repr__(self):
        return "$(" + repr(self.stream) + ")"
    
    def eval(self, context):
        subcontext = context.enter_scope()
        items = list(self.stream.eval(subcontext))
        if len(items) == 1:
            return items[0]
        return PiperList(items)


class Variable:
    def __init__(self, name: str, srcpos: SourcePos):
        self.name = name
        self.srcpos = srcpos

    def __repr__(self):
        return self.name
    
    def eval(self, context):
        value = context.get_variable(self.name)
        if value is None:
            raise PiperError(f"Variable '{self.name}' is not defined", [self.srcpos])
        return value


class Assign:
    def __init__(self, var, value, srcpos: SourcePos):
        self.var = var
        self.value = value
        self.srcpos = srcpos

    def __repr__(self):
        return repr(self.var) + ' = ' + repr(self.value)
    
    def eval(self, context):
        context.bind_variable(self.var.name, self.value.eval(context))


class RedirectOutput:
    def __init__(self, var: Variable, srcpos: SourcePos):
        self.var = var
        self.srcpos = srcpos

    def __repr__(self):
        return f">{repr(self.var)}"
    
    def eval(self, context, prev_generator):
        items = []
        for item in prev_generator:
            yield item
            items.append(item)
        context.bind_variable(self.var.name, PiperList(items))


class RedirectInput:
    def __init__(self, input_, srcpos: SourcePos):
        self.input_ = input_
        self.srcpos = srcpos

    def __repr__(self):
        return f"{repr(self.input_)} >"
    
    def eval(self, context, prev_generator):
        # RedirectInput must always be first in the stream
        assert prev_generator == None

        value = self.input_.eval(context)
        if isinstance(value, PiperList):
            yield from value._piper_iter()
        else:
            yield value


class Argument:
    def __init__(self, name: str, value, srcpos: SourcePos):
        self.name = name
        self.value = value
        self.srcpos = srcpos

    def __repr__(self):
        if self.name:
            return f"{self.name}={repr(self.value)}"
        else:
            return f"{repr(self.value)}"

    def eval(self, context):
        return Argument(self.name, self.value.eval(context), self.srcpos)


class Command:
    def __init__(self, actor_name: str, args: [Argument], srcpos: SourcePos):
        self.actor_name = actor_name
        self.args = args
        self.srcpos = srcpos

    def __repr__(self):
        if self.args:
            args = " ".join(map(repr, self.args))
            return f"{self.actor_name} {args}"
        else:
            return f"{self.actor_name}"

    def eval(self, context, prev_generator):
        args = [arg.eval(context) for arg in self.args]
        actor = context.get_actor(self.actor_name)
        
        if actor is None:
            raise PiperError(f"No '{self.actor_name}' found", [self.srcpos])
        
        if prev_generator is None and not actor.is_producer:
            raise PiperError(f"The actor '{self.actor_name}' is incapable of producing elements", [self.srcpos])
        elif prev_generator is not None and not actor.is_transformer:
            raise PiperError(f"The actor '{self.actor_name}' is incapable of transforming elements", [self.srcpos])

        cmd_cntxt = context.enter_scope()
        try:
            yield from actor.eval(cmd_cntxt, args, prev_generator, self.srcpos)
        except PiperError as e:
            raise e
        except PiperUnexpectedType as e:
            raise PiperError(f"Expected type '{e.expected._piper_type_name}' but got type '{e.got._piper_type_name}'", [self.srcpos])
        except PiperRuntimeError as e:
            raise PiperError(f"Runtime error during evaluation: {e}", [self.srcpos])
        except Exception as e:
            exc = traceback.format_exc()
            raise PiperError(f"{exc}\nAn unexpected error occured during evaluation, please fill a bug report.", [self.srcpos])


class Lambda:
    def __init__(self, args, body, srcpos):
        self.args = args
        self.body = body
        self.srcpos = srcpos
    
    def __repr__(self):
        return repr(self.body)
    
    def eval(self, context):
        def lambda_(*arg_values):
            if len(arg_values) != len(self.args):
                raise PiperError(f'Defined lambda requires {len(self.args)} argument(s) but {len(arg_values)} was provided', [self.srcpos])
            
            lambda_context = context.enter_scope()
            for arg, value in zip(self.args, arg_values):
                lambda_context.bind_variable(arg.name, value)
            return self.body.eval(lambda_context)
        return PiperLambda(lambda_)


class If:
    def __init__(self, cond, then, else_, srcpos):
        self.cond = cond
        self.then = then
        self.else_ = else_
        self.srcpos = srcpos
    
    def __repr__(self):
        return f"If({repr(self.cond)} then {repr(self.then)} else {repr(self.else_)})"
    
    def eval(self, context):
        if self.cond.eval(context):
            return self.then.eval(context)
        else:
            return self.else_.eval(context)


class BinOp:
    def __init__(self, op, op_repr, left, right, srcpos=None):
        self.op = op
        self.op_repr = op_repr
        self.left = left
        self.right = right
        self.srcpos = srcpos or left.srcpos.extend_to(right.srcpos)
    
    def __repr__(self):
        return repr(self.left) + ' ' + self.op_repr + ' ' + repr(self.right)

    def eval(self, context):
        try:
            return self.op(self.left.eval(context), self.right.eval(context))
        except BinaryOpWrongType as e:
            raise PiperError(f"Incompatible operation between '{e.left._piper_type_name}' and '{e.right._piper_type_name}'", [self.srcpos])


class AndOp(BinOp):
    def __init__(self, left, right):
        op = lambda l, r: PiperBool(l._piper_as_bool() and r._piper_as_bool())
        super().__init__(op, 'and', left, right)


class OrOp(BinOp):
    def __init__(self, left, right):
        op = lambda l, r: PiperBool(l._piper_as_bool() or r._piper_as_bool())
        super().__init__(op, 'or', left, right)


class UnOp:
    def __init__(self, op, op_repr, value, srcpos):
        self.op = op
        self.op_repr = op_repr
        self.value = value
        self.srcpos = srcpos
    
    def __repr__(self):
        return self.op_repr + ' ' + repr(self.value)

    def eval(self, context):
        return self.op(self.value.eval(context))


class Index:
    def __init__(self, list_, at, srcpos):
        self.list_ = list_
        self.at = at
        self.srcpos = srcpos
    
    def __repr__(self):
        return f"{repr(self.list_)}[{repr(self.at)}]"
    
    def eval(self, context):
        try:
            return self.list_.eval(context)[self.at.eval(context)]
        except WrongIndexType as e:
            raise PiperError(f"Index type '{e.at._piper_type_name}' is a not a valid index", [self.srcpos])
        except WrongIndexValue as e:
            raise PiperError(f"Index value '{e.at}' is a not a valid index", [self.srcpos])


class AccessField:
    def __init__(self, object_, field, srcpos):
        self.object_ = object_
        self.field = field
        self.srcpos = srcpos
    
    def __repr__(self):
        return f"{repr(self.object_)}.{repr(self.field)}"
    
    def eval(self, context):
        if self.field.startswith('_piper') or self.field.startswith('__'):
            raise PiperError(f"Fields starting with '__' or '_piper' are reserved", [self.srcpos])
        try:
            value = getattr(self.object_.eval(context), self.field)
            if is_python_value(value):
                return wrap_python_value(value)
            else:
                return value
        except AttributeError:
            raise PiperError(f"No field named '{self.field}' exists for '{repr(self.object_)}'", [self.srcpos])


class Call:
    def __init__(self, callee, args, srcpos):
        self.callee = callee
        self.args = args
        self.srcpos = srcpos
    
    def __repr__(self):
        args = ",".join(repr(arg for arg in self.args))
        return f"{repr(self.callee)}({args})"
    
    def eval(self, context):
        callee = self.callee.eval(context)
        args = [arg.eval(context) for arg in self.args]
        try:
            return callee(*args)
        except TypeError as e:
            print(e)
            raise PiperError(f'Lambda called with the wrong number of arguments', [self.srcpos])


class Literal:
    def __init__(self, value: str, srcpos: SourcePos):
        self.value = value
        self.srcpos = srcpos

    def __repr__(self):
        return str(self.value)

    def eval(self, context):
        return self.piper_class(self.value)


class Number(Literal):
    piper_class = PiperNumber


class String(Literal):
    piper_class = PiperString


class Bool(Literal):
    piper_class = PiperBool


class Path(Literal):
    piper_class = PiperPath


class Object:
    def __init__(self, fields: [tuple], srcpos: SourcePos):
        self.fields = fields
        self.srcpos = srcpos

    def __repr__(self):
        return repr(self.fields)

    def eval(self, context):
        fields = {}
        for name, value in self.fields:
            fields[name] = value.eval(context)
        return PiperObject(fields)


class List:
    def __init__(self, values, srcpos: SourcePos):
        self.values = values
        self.srcpos = srcpos

    def __repr__(self):
        return repr(self.values)

    def eval(self, context):
        return PiperList([value.eval(context) for value in self.values])

class PiperTransformer(Transformer):
    def program(self, program):
        return Program(program)

    def stream(self, stream):
        return Stream(stream)

    def substream(self, substream):
        tkn_open, ss, tkn_close = substream
        srcpos = SourcePos(tkn_open).extend_to(SourcePos(tkn_close))
        return SubStream(ss, srcpos)

    def command(self, cmd):
        tkn_actor, *args = cmd
        actor = tkn_actor.value
        return Command(actor, args, SourcePos(tkn_actor))

    def implicit_lambda(self, implicit_lambda):
        expr = implicit_lambda[0]
        x = Variable('$x', expr.srcpos)
        lambda_ = Lambda([x], expr, expr.srcpos)
        arg = Argument(None, lambda_, expr.srcpos)
        return Command('apply', [arg], expr.srcpos)

    def actor(self, actor):
        return actor[0]

    def short_arg(self, short_arg):
        return short_arg[0]

    def long_arg(self, long_arg):
        return long_arg[0]

    def switch_arg(self, switch_arg):
        tkn = switch_arg[0]
        return Argument(tkn.value, Bool(True, tkn), SourcePos(tkn))

    def named_arg(self, named_arg):
        tkn_name = named_arg[0]
        value = named_arg[1]
        srcpos = SourcePos(tkn_name).extend_to(value.srcpos)
        return Argument(tkn_name.value, value, srcpos)

    def positional_arg(self, p_arg):
        value = p_arg[0]
        return Argument(None, value, value.srcpos)

    def var(self, var):
        tkn_var = var[0]
        return Variable(tkn_var.value, SourcePos(tkn_var))

    def redirect_output(self, redirect_output):
        var = redirect_output[0]
        return RedirectOutput(var, var.srcpos)
    
    def redirect_input(self, redirect_input):
        var = redirect_input[0]
        return RedirectInput(var, var.srcpos)
    
    def add(self, add):
        return BinOp(operator.add, '+', add[0], add[1])
    
    def sub(self, sub):
        return BinOp(operator.sub, '-', sub[0], sub[1])
    
    def mul(self, mul):
        return BinOp(operator.mul, '*', mul[0], mul[1])
    
    def int_div(self, int_div):
        return BinOp(operator.floordiv, '//', int_div[0], int_div[1])
    
    def true_div(self, true_div):
        return BinOp(operator.truediv, '/', true_div[0], true_div[1])
    
    def equal(self, equal):
        return BinOp(operator.eq, '==', equal[0], equal[1])
    
    def not_equal(self, not_equal):
        return BinOp(operator.ne, '!=', not_equal[0], not_equal[1])
    
    def lower(self, lower):
        return BinOp(operator.lt, '<', lower[0], lower[1])
    
    def lower_equal(self, lower_equal):
        return BinOp(operator.le, '<=', lower_equal[0], lower_equal[1])
    
    def greater(self, greater):
        return BinOp(operator.gt, '>', greater[0], greater[1])
    
    def greater_equal(self, greater_equal):
        return BinOp(operator.ge, '>=', greater_equal[0], greater_equal[1])
    
    def power(self, power):
        return BinOp(operator.pow, '^', power[0], power[1])
    
    def if_(self, if_):
        tkn_if, cond, tkn_then, then, tkn_else, else_ = if_
        srcpos = SourcePos(tkn_if).extend_to(else_.srcpos)
        return If(cond, then, else_, srcpos)
    
    def unary_minus(self, unary_minus):
        tkn_sign, value = unary_minus
        srcpos = SourcePos(tkn_sign).extend_to(value.srcpos)
        return UnOp(operator.neg, '-', value, srcpos)
    
    def index(self, index):
        list_, tkn_open_bracket, at, tkn_close_bracket = index
        srcpos = SourcePos(tkn_open_bracket).extend_to(SourcePos(tkn_close_bracket))
        return Index(list_, at, srcpos)
    
    def dot(self, dot):
        object_, tkn_name = dot
        srcpos = object_.srcpos.extend_to(SourcePos(tkn_name))
        return AccessField(object_, tkn_name.value, srcpos)
    
    def and_(self, and_):
        return AndOp(and_[0], and_[1])
    
    def or_(self, or_):
        return OrOp(or_[0], or_[1])

    def not_(self, not_):
        not_tkn, value = not_
        op = lambda val: not val._piper_as_bool()
        return UnOp(op, 'not', value, SourcePos(not_tkn).extend_to(value.srcpos))
    
    def string(self, string):
        expr_tree = None
        for part in string:
            if isinstance(part, Token):
                value = String(deescape_string(part.value), SourcePos(part))
            else:
                value = part
            
            if expr_tree is None:
                expr_tree = value
            else:
                expr_tree = BinOp(operator.add, '', expr_tree, value)
        return expr_tree
    
    def interpolation_expr(self, interpolation_expr):
        tkn_open, expr, tkn_close = interpolation_expr
        before = String(deescape_string(tkn_open.value[:-1]), SourcePos(tkn_open))
        after = String(deescape_string(tkn_close.value[1:]), SourcePos(tkn_close))
        expr = UnOp(lambda x: PiperString(x._piper_format()), '', expr, expr.srcpos)
        before_expr = BinOp(operator.add, '', before, expr, srcpos=expr.srcpos)
        return BinOp(operator.add, '', before_expr, after, srcpos=expr.srcpos)

    def bool_(self, bool_):
        tkn_bool = bool_[0]
        return Bool(tkn_bool.value == 'true', SourcePos(tkn_bool))

    def number(self, number):
        tkn_number = number[0]
        return Number(tkn_number.value.replace('_', ''), SourcePos(tkn_number))
    
    def path(self, path):
        if len(path) == 2:
            # path as p"/path/here"
            tkn_p, tkn_path = path
            value = tkn_path.value[1:-1]
            srcpos = SourcePos(tkn_p).extend_to(SourcePos(tkn_path))
        else:
            # path as /path/here
            tkn_path = path[0]
            value = tkn_path.value
            srcpos = SourcePos(tkn_path)

        return Path(value, SourcePos(tkn_path))
    
    def lambda_(self, lambda_):
        open_par, *args, body, close_par = lambda_
        srcpos = SourcePos(open_par).extend_to(SourcePos(close_par))
        return Lambda(args, body, srcpos)
    
    def object(self, object):
        srcpos = SourcePos(object[0]).extend_to(SourcePos(object[-1]))

        fields = []
        for field in object[1:-1]:
            if isinstance(field, AccessField):
                # copy field
                name = field.field
                value = field
            else:
                name, value = field.children
                name = name.value
            fields.append((name, value))
        
        return Object(fields, srcpos)
    
    def list_(self, list_):
        srcpos = SourcePos(list_[0]).extend_to(SourcePos(list_[-1]))
        values = list_[1:-1]
        return List(values, srcpos)

    def assign(self, assign):
        var, value = assign
        srcpos = var.srcpos.extend_to(value.srcpos)
        return Assign(var, value, srcpos)
    
    def call(self, call):
        callee, tkn_open_paren, *args, tkn_close_paren = call
        srcpos = callee.srcpos.extend_to(SourcePos(tkn_close_paren))
        return Call(callee, args, srcpos)
