from lark import Lark

from transformer import PiperTransformer

grammar = """
%import common.CNAME
%import common.LETTER
%import common.NUMBER
%import common.DIGIT
%import common.ESCAPED_STRING
%import common.WS_INLINE
%import common.WS
%import common.NEWLINE

%ignore WS_INLINE
%ignore COMMENT
%ignore IGNORE_NEWLINE

COMMENT: /#[^\\n]*/
IGNORE_NEWLINE: "\\\\" NEWLINE

?name:     /[a-zA-Z_][a-zA-Z0-9_]*/
var:       /\$[a-zA-Z_][a-zA-Z0-9_]*/
actor:     name

bool_:     (/true/|/false/)
number:    /[-+]?([0-9][0-9_]*(\\.[0-9_]*)?|\\.[0-9_]+|inf)/
path:      /(\\.\\.\\/|\\.\\/|\\/)[^\\s\\{\\}|\\=!<>\\^\\$]*/
    |      /p/ ESCAPED_STRING
field:     name "=" expr
?copy_field: dot | field
object:    /\\{/ (copy_field ",")* copy_field? /\\}/
list_:      /\\[/ (expr ",")* expr? /\\]/

?inner_string: /(\\\\[\\\\nt"{}]|[^\\\\{}"])+/
    | /\s*{/ expr /}\s*/ -> interpolation_expr
string: "\\"" inner_string* "\\""

_pipe: "|"


?expr: not_

?not_: or_
    | /not/ not_

?or_: and_
    | or_ "or" and_

?and_: comparaison
    | and_ "and" comparaison

?comparaison: sum
    | sum "==" sum  -> equal
    | sum "!=" sum  -> not_equal
    | sum "<" sum   -> lower
    | sum "<=" sum  -> lower_equal
    | sum ">" sum   -> greater
    | sum ">=" sum  -> greater_equal

?sum: product
    | sum "+" product -> add
    | sum "-" product -> sub

?product: power
    | product "*" power  -> mul
    | product "/" power  -> true_div
    | product "//" power -> int_div

?power: unary_minus
    | unary_minus "^" power

?unary_minus: call_index_dot
    | /-/ unary_minus

dot: call_index_dot "." CNAME
?call_index_dot: if_
    | call_index_dot /\\(/ (expr ",")* expr? /\\)/ -> call
    | call_index_dot /\\[/ expr /\\]/              -> index
    | dot

?if_: atom
    | /if/ expr /then/ expr /else/ expr

?atom: literal
    | var
    | lambda_
    | "(" expr ")"
    | /\\$\\(/ stream /\\)/  -> substream

?literal: number
    | string
    | bool_
    | path
    | object
    | list_


lambda_: /\\(/ (var ",")* var? ":" expr /\\)/
?parent_expr: "(" expr ")"

?arg_value: (var | literal | parent_expr | lambda_)
short_arg:  "-" /[a-zA-Z]/
long_arg:   "--" /[a-zA-Z_][a-zA-Z0-9_\\-]*/
named_arg:  (short_arg | long_arg) "="? arg_value
switch_arg: (short_arg | long_arg)
positional_arg: arg_value

command: actor positional_arg* (named_arg | switch_arg)*
redirect_output: ">>" var
redirect_input: expr ">>"
implicit_lambda: expr

_stream_input: (redirect_input (command | implicit_lambda) | command)
stream: _stream_input (_pipe "\\n"* (command | implicit_lambda))* redirect_output?

assign: var "=" expr

?line: stream | assign
_newline: ("\\r"? "\\n")+
program: 
    | _newline? (line _newline)* line _newline?
"""


def parse(content: str):
    parser = Lark(grammar, start="program")
    tree = parser.parse(content)
    trans = PiperTransformer()
    return trans.transform(tree)
