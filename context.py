import sys
from collections import defaultdict

from actor import get_registered_actors
from piper_types import PiperList, wrap_python_list


class Scope:
    def __init__(self):
        self.variables = {}
        self.actors = {}


class Context:
    def __init__(self, scopes=None):
        self.scopes = scopes or [Scope()]

    def enter_scope(self):
        return Context(self.scopes + [Scope()])

    def exit_scope(self):
        return Context(self.scopes[:-1])

    def bind_variable(self, name, value):
        self.scopes[-1].variables[name] = value

    def bind_actor(self, name, actor):
        self.scopes[-1].actors[name] = actor

    def get_variable(self, name):
        n = len(self.scopes)
        for i in range(n):
            s = self.scopes[n - 1 - i]
            val = s.variables.get(name)
            if val is not None:
                return val
        return None

    def get_actor(self, name):
        n = len(self.scopes)
        for i in range(n):
            s = self.scopes[n - 1 - i]
            actor = s.actors.get(name)
            if actor is not None:
                return actor
        return None


def get_builtin_context():
    # actors register themselves but we need to make sure they are imported
    import builtin_actors

    actors = get_registered_actors()

    c = Context()
    for name, actor in actors.items():
        c.bind_actor(name, actor)
    
    c.bind_variable('$args', wrap_python_list(sys.argv[1:]))
    
    return c
