{ pkgs ? import <nixpkgs> {} }:

pkgs.python3Packages.buildPythonApplication {
    pname = "pipeline";
    src = ./.;
    version = "0.1";
    propagatedBuildInputs = with pkgs.python3Packages; [ lark-parser ];
}