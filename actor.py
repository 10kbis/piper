import inspect

from exceptions import ErroneousActorDefinition, PiperError
from piper_types import *


class Actor:
    def __init__(self, name, is_producer, is_transformer, description, func_generator):
        self.name = name
        self.is_producer = is_producer
        self.is_transformer = is_transformer
        self.description = description
        self.func_generator = func_generator
        self.args = []
        self.long_names = dict()
        self.short_names = dict()
        self.positional_args = []
        self.named_args = {}

    def _check_nb_args_in_signature(self):
        sign = inspect.signature(self.func_generator)
        if self.is_transformer and "stdin" not in sign.parameters:
            raise ErroneousActorDefinition(
                self.name,
                f"Every transformer Actor must accept an stdin argument (that must not be declared with a decorator)",
            )

        # + self.is_transformer because of the stdin argument that all transformers must have
        if len(sign.parameters) != len(self.args) + self.is_transformer:
            raise ErroneousActorDefinition(
                self.name,
                f"The number of defined arguments through the decorators does not match the number of argument of the Python function",
            )

    def add_arg(self, arg):
        sign = inspect.signature(self.func_generator)
        if arg.long_name not in sign.parameters:
            raise ErroneousActorDefinition(
                self.name,
                f"Argument '{arg.long_name}' not in signature of the actor '{self.name}' Python function.",
            )

        if type(arg.type_) != type and not issubclass(arg.type_, PiperAny):
            raise ErroneousActorDefinition(
                self.name,
                f"Type for argument '{arg.long_name}' is '{arg.type_}' but is not a real type.",
            )

        if not issubclass(arg.type_, PiperAny) and arg.type_ not in python_piper_type_mapping:
            raise ErroneousActorDefinition(
                self.name,
                f"Type for argument '{arg.long_name}' is '{arg.type_.__name__}' but only subclasses of PiperAny or wrappable Python types are valid types.",
            )

        if arg.default is not None and not issubclass(type(arg.default), arg.type_):
            got_type = type(arg.default).__name__
            exp_type = arg.type_.__name__
            raise ErroneousActorDefinition(
                self.name,
                f"Default value for argument '{arg.long_name}' is of type '{got_type}' but expected type '{exp_type}'.",
            )

        if arg.long_name in self.long_names:
            raise ErroneousActorDefinition(
                self.name, f"Trying to redefined argument '{arg.long_name}'."
            )

        if arg.short in self.short_names:
            raise ErroneousActorDefinition(
                self.name, f"Trying to redefined short argument '{arg.short}'."
            )

        if arg.short is not None and arg.positional:
            raise ErroneousActorDefinition(
                self.name,
                f"Trying to define a positional argument '{arg.long_name}' with short name '{arg.short}' but positional arguments can not have short names.",
            )

        if not arg.positional and arg.default is None:
            raise ErroneousActorDefinition(
                self.name,
                f"Trying to define a named argument '{arg.long_name}' but no default value has been provided.",
            )

        if self.args:
            prev_arg = self.args[-1]
            if not prev_arg.positional and arg.positional:
                raise ErroneousActorDefinition(
                    self.name,
                    f"Trying to define argument '{arg.long_name}' as positional but previous argument must also be positional.\n"
                    "Positional arguments must be defined before any non positional argument.",
                )
            elif (
                prev_arg.positional
                and prev_arg.default is not None
                and arg.positional
                and arg.default is None
            ):
                raise ErroneousActorDefinition(
                    self.name,
                    f"Trying to define a required positional argument '{arg.long_name}' but previous argument must also be required.\n"
                    "Required positional arguments must be defined before any optional positional argument.",
                )

        self.args.append(arg)
        self.long_names[arg.long_name] = arg
        if arg.short is not None:
            self.short_names[arg.short] = arg

        if arg.positional:
            self.positional_args.append(arg)
        else:
            self.named_args[arg.long_name] = arg

    def __repr__(self):
        args = ", ".join(map(str, self.args))
        return f"Actor({self.name}, [{args}])"

    def eval(self, context, args, prev_generator, srcpos_actor):
        p_args = [arg for arg in args if arg.name is None]
        named_args = [arg for arg in args if arg.name is not None]

        # arguments to pass to the actor's func_generator
        actor_args = {
            arg.long_name: arg.default for arg in self.named_args.values()
        }

        used_args = dict()

        if len(p_args) > len(self.positional_args):
            print(p_args)
            srcpos = [arg.srcpos for arg in p_args[len(self.positional_args):]]
            n = len(self.positional_args)
            m = len(p_args)
            raise PiperError(
                f"Too many positional arguments. Excpected maxium {n} arguments but {m} were given",
                srcpos,
            )

        for i, arg in enumerate(self.positional_args):
            name = arg.long_name
            if i >= len(p_args):
                if arg.default is None:
                    raise PiperError(f"Argument '{name}' missing", [srcpos_actor])
                value = arg.default
            else:
                p_arg = p_args[i]
                value = p_arg.value
                used_args[arg.long_name] = p_arg

            actor_args[name] = value

        for n_arg in named_args:
            name = n_arg.name
            if len(name) == 1:
                arg = self.short_names.get(name)
            else:
                arg = self.long_names.get(name)

            if arg is None:
                raise PiperError(
                    f"'{self.name}' has no argument named '{name}'", [n_arg.srcpos]
                )

            if arg.long_name in used_args:
                prev_used_arg = used_args[arg.long_name]
                raise PiperError(
                    f"Argument '-{arg.short} --{arg.long_name}' has already been passed",
                    [prev_used_arg.srcpos, n_arg.srcpos],
                )

            actor_args[arg.long_name] = n_arg.value
            used_args[arg.long_name] = n_arg

        # Convert types from Python types to Piper types and vice versa
        for name, value in actor_args.items():
            arg = self.long_names[name]
            is_src_py = is_python_type(type(value))
            is_dst_py = is_python_type(arg.type_)

            if is_src_py and not is_dst_py:
                value = wrap_python_value(value)
            elif not is_src_py and is_dst_py:
                value = value._piper_unwrap()

            actor_args[name] = value

        if self.is_transformer:
            actor_args["stdin"] = prev_generator

        for item in self.func_generator(**actor_args):
            if is_python_value(item):
                yield wrap_python_value(item)
            else:
                yield item


class Argument:
    def __init__(self, long_name, type_, description, default, short, positional):
        self.long_name = long_name
        self.type_ = type_
        self.description = description
        self.default = default
        self.short = short
        self.positional = positional

    def __str__(self):
        return f"{self.long_name}"

    def __eq__(self, other):
        return (
            self.long_name == other.long_arg
            and self.type_ == other.type_
            and self.description == other.description
            and self.default == other.default
            and self.short == other.short
            and self.positional == other.positional
        )

    def __hash__(self):
        return hash(
            (
                self.long_name,
                self.type_,
                self.description,
                self.default,
                self.short,
                self.positional,
            )
        )


_actors = {}
_actors_func_name = {}


def actor(name, description, is_producer, is_transformer):
    def inner(actor_func):
        func_name = actor_func.__name__
        if name is None:
            actor_name = func_name
        else:
            actor_name = name
            
        if actor_name in _actors:
            raise ErroneousActorDefinition(actor_name, f"Trying to redefine actor '{actor_name}'.")
        actr = Actor(actor_name, is_producer, is_transformer, description, actor_func)
        _actors[actor_name] = actr
        _actors_func_name[func_name] = actr
        return actor_func

    return inner

producer = lambda description, name=None: actor(name, description, is_producer=True, is_transformer=False)
transformer = lambda description, name=None: actor(name, description, is_producer=False, is_transformer=True)
trans_producer = lambda description, name=None: actor(name, description, is_producer=True, is_transformer=True)


def arg(long_name, type_, description, default=None, short=None, positional=False):
    argument = Argument(long_name, type_, description, default, short, positional)

    def inner(actor_func):
        name = actor_func.__name__
        actor = _actors_func_name.get(name)
        if actor is None:
            raise ErroneousActorDefinition(
                name,
                f"Function {name} not declared has an Actor. Please add the decorator @producer/transformer(...) as the first decorator of the function.",
            )
        actor.add_arg(argument)
        return actor_func

    return inner


_checked_actors = False


def get_registered_actors():
    global _checked_actors
    if not _checked_actors:
        # It's not possible to check during the Actor creation if the number of declared
        # arguments through decorators is the same as the number of arguments of the
        # function
        for actor in _actors.values():
            actor._check_nb_args_in_signature()
        _checked_actors = True
    return _actors
