Piper is a work in progress scripting language. Piper aims at being modern scripting language.

# Install
Piper requires `Python3` and the library `lark-parser`


# Getting started
The only way to use Piper as of now is to write your script in a file and invoke it with

    python main.py /path/to/your/script.pp

Piper is based on two concepts streams and actors. Actors produce and transform values and streams move those values from one actor to another.

## Actors

Actors are separated into two categories: producers and transformers. A producer can only output values and as such must be used a the beginning of a stream. Transformers must consume values to produce new ones and cannot be used at the beginning. An actor can be both a producer and a transformer based on its arguments.

For example, probably the simplest producer (list the content of the current directory)

    ls

Note that `ls` is a program in UNIX distributions, Piper does not use that program. The name is the same only for the learning curve to not be too steep.

Actors can have arguments, they may be switches or values. Switches always have a long name (starts with `--`) and optionally a short name (starts with `-`)

    ls --files # list only files
    ls -f # Short version, equivalent to --files

Some actors have positional arguments, them must always come before optional arguments.

    # range <start> <stop> <--step=1>
    range 0 10 # 0 1 2 3 4 5 6 7 8 9
    range 0 10 --step 2 # valid
    range 0 --step 2 10 # invalid

Missing arguments that are required result in an error

    range 10
     1 | range 10
         ^^^^^
    Argument 'stop' missing

There is no documentation yet but actors are defined in `builtin_actors.py`. The arguments definition should be relatively readable even for non Python devs.

Literals can be used as argument but if some operation must be done, the expression must be surrounded by parenthesis.

    range 0 1 + 2 # invalid
    range 0 (1 + 2) # valid


## Streams

Streams are similar to shell streams, they send the value from one actor to the next one. Actors are connected with the symbol `|`.

    ls | sort --reverse

### Substream

Streams can be nested with `$(...)` (See anonymous Lambda for the use of `$x`).

    range 15 20 | 2 ^ $x | $($x >> unit "storage_binary")

Output

    32 KiB
    64 KiB
    128 KiB
    256 KiB
    512 KiB


## Types

Piper has the following built-in types: Number, Bool, String, List, Path, Object and Lambda. Every value in Piper is immutable.

### Number

Numbers can represent integer or floating point value. If a number is not an integer it is automatically promoted to a decimal (see [specification](http://speleotrove.com/decimal/decarith.html)).


    $num = 1 # Number can represent Integer
    $num2 = 1.2 # Decimal
    $num3 = 1 / 2 # Promoted to a Decimal
    $num3 = 0.1 * 3 # is equal to 0.3 not 0.30000000000000004

### Bool
    
    $mybool = True
    $mybool = False


### String

    $str = "Hello World!"
    $str.length() # 12
    $repeat = "abc" * 3 # "abcabcabc"
    $add = "Hell" + "o" # "Hello"
    $str = "{$add} = {$add.length()} characters" # "Hello = 5 characters"

### List

Lists can contain any number of values of any type

    $mylist = [123, 1.5, "Hi there!"]
    $mylist[1] # 1.5
    $mylist.length() # 3


### Path

Paths are also literals like Numbers, Bools, etc. They must start with a `./` or `/`. If there is a special character in them, they must be surrounded with `"` and prefixed by `p`.

    $current_dir = ./
    $file = ./todo.txt
    $absolute_dir = /home/chloe
    $with_space = p"./videos/strange loop conf 2019.mp4"

    $file.is_file() # True
    $file.size # Return the size of the file. E.g. 456


### Object

Objects map key to value, they are similar to json objects

    $temperature = {date="2021-05-19", temps=[15.5, 16.2, 18.1]}
    $temperature.date # "2021-05-19"
    $temperature["date"] # "2021-05-19"
    
When creating a new Object from an another one, fields are often reused. Piper comes with syntactic sugar for such cases

    $alan = {name="Alan", city="Paris"}
    $new_alan = {$alan.name, age = 21} # {name="Alan", age=21}

### Lambda

Lambdas are functions, they always evaluate to an expression.

    # The actor 'select' only output the value if the predicate is true.
    ls | select ($path: $path.ext == ".py")

When the arguments of the Lambda are omitted, the Lambda is anonymous. An anonymous always take a single argument named $x. This notation is syntaxic sugar for `apply ($arg: ...)`.

    ls -f | "file: {$x}"
    ls -f | apply ($f: "file: {$f}") # equivalent

## Redirection

Streams can be redirected to variable, a redirection does not consume the values so it is possible to redirect in the middle of a stream.

    ls -f >> $files # Build a List with the output of ls

Variables can also be used instead of a producer (see Actors).

    $fib = [1, 1, 2, 3, 5]
    $fib >> sum



# Examples

## List input arguments

    # python main.py script.pp hello there
    enum $args

Output

    {i = 0, value = hello}
    {i = 1, value = there}

## Files by extension and sort by size
Show the number of files and their sizes grouped by their extensions, sorted by the sum of the sizes

    ls -f |
        groupby ($f: $f.ext) --key "ext" --values "files" |
        { $x.ext, count = $x.files.length(), size = $($x.files >> $x.size | sum) } |
        sort ($grp: $grp.size) |
        "ext={$x.ext}, {$($x.size >> unit "storage")} ({$x.count} files)"
    
Output for this repository

    ext=, 12 B (1 files)
    ext=.nix, 212 B (1 files)
    ext=.pp, 941 B (1 files)
    ext=.md, 4.6 kB (1 files)
    ext=.py, 61.7 kB (8 files)

## Write lines

    count 10 | writelines ./count.txt | {n=2 ^ $x, i=$x} | writelines ./end.txt

count.txt contains

    0
    1
    2
    3
    4
    5
    6
    7
    8
    9

And end.txt contains

    {n = 1, i = 0}
    {n = 2, i = 1}
    {n = 4, i = 2}
    {n = 8, i = 3}
    {n = 16, i = 4}
    {n = 32, i = 5}
    {n = 64, i = 6}
    {n = 128, i = 7}
    {n = 256, i = 8}
    {n = 512, i = 9}
